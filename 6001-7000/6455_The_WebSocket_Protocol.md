> 相关更新：[7936](https://gitee.com/keyvuui/rfc_reference_ZH_CN.git) [8307] (https://gitee.com/keyvuui/rfc_reference_ZH_CN.git) 					
*已提出的标准*

```
Internet Engineering Task Force (IETF)                          *I. Fette*
Request for Comments: 6455                                  *Google, Inc.*
Category: Standards Track                                    *A. Melnikov*
ISSN: 2070-1721                                               *Isode Ltd.*
                                                           *December 2011*
```

# The WebSocket Protocol

## 简介

&emsp;&emsp;**WebSocket** 协议实现了客户端和服务器的双向通信。客户端在可控的环境中运行程序，服务器设定好与客户端的程序进行通信。客户端和服务器所使用的安全模型是浏览器原生、现在广泛使用的一个模型。这个协议由一个最开始的握手报文和随之而来的基本信息帧组成，基于 **TCP** 传输（属于应用层协议）。这项技术的目标是提供一种机制，使得需要双边通信、并且基于浏览器的应用不必于建立多个 **HTTP** 连接。（例如，使用 **XMLHTTPRequest** 或者 **< iframe >** 和长轮询）。

## 这个文档的现状

&emsp;&emsp;这是一个 **Internet Standards Track** 文档。

&emsp;&emsp;这篇文档是 **IETF** 的作品。

## 版权信息

## 1. 介绍

### 1.1 背景

&emsp;&emsp;*这一部分是不规范的*

&emsp;&emsp;以前，创建一个 **web** 应用需要客户端和服务器之间进行双边通信（例如，即时通信软件和游戏），并且当服务器以 **HTTP** 形式发送一个来自上游的通知时，客户端需要要建立一个 **HTTP** 连接来轮询这个更新。[[RFC6202]()]

&emsp;&emsp;这样就带来了很多问题：

- 服务器需要给每一个客户端建立很多个底层的 **TCP** 连接：一个用来给客户端发送信息，一个新的用来接收信息。
- 这个连接协议有很高的开销，因为每一个服务器和客户端之间的消息都有一个 **HTTP Header**。
- 客户端的脚本需要维护一个出向连接和入向连接的映射，这个映射是为了跟踪回复信息。

&emsp;&emsp;一个简单的解决方案或许是在同一个信息传送方向上只使用一个 **TCP** 连接来通信。这就是 **WebSocket** 协议所提供的。利用 **WebSocket API** [[WSAPI]()]，在实现网页和远端服务器双边通信时，我们有了一种可替代 **HTTP** 长轮询的解决方案。

&emsp;&emsp;相似的技术可以被用在多种多样的 **web** 应用中：游戏、股票监控、多用户同步编辑的应用、服务端的实时服务接口、等等。

